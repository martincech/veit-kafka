﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using Xunit;
using Moq;
using Veit.Kafka;
using Veit.Kafka.Topics;
using Veit.Kafka.Model;
using Veit.Kafka.Cache;

namespace KafkaBatLibrary.Tests
{
    public class KafkaSenderTests
    {
        readonly KafkaSender kafkaSender;
        readonly List<DailyWeight> dailyWeights;

        readonly BlockingCollection<KafkaMessage> queue;

        readonly Mock<IProducer> producerMock;
        readonly Mock<ICachedQueue<KafkaMessage>> cacheMock;

        private readonly CancellationTokenSource cancellationTokenSource;

        public KafkaSenderTests()
        {
            producerMock = new Mock<IProducer>();
            queue = new BlockingCollection<KafkaMessage>();
            cacheMock = new Mock<ICachedQueue<KafkaMessage>>();
            cancellationTokenSource = new CancellationTokenSource();
            cacheMock.Setup(s => s.GetConsumingEnumerable(It.IsAny<CancellationToken>())).Returns(queue.GetConsumingEnumerable(cancellationTokenSource.Token));
            cacheMock.Setup(s => s.Add(It.IsAny<KafkaMessage>()))
               .Callback<KafkaMessage>(item => queue.Add(item));

            kafkaSender = new KafkaSender(producerMock.Object, cacheMock.Object, cancellationTokenSource);

            dailyWeights = new List<DailyWeight>
             {
                new DailyWeight{ Average = 1, Count=2, Cv = 3, Day = 4, Gain = 5, Sex = "male", Sigma = 6, TimeStamp= DateTime.Now.AddDays(-1), Uid = "D1-T-T1", Uniformity=7 },
                new DailyWeight{ Average = 2, Count=3, Cv = 4, Day = 5, Gain = 6, Sex = "female", Sigma = 7, TimeStamp= DateTime.Now.AddDays(-1), Uid = "D1-T-T1", Uniformity=8 },
                new DailyWeight{ Average = 3, Count=4, Cv = 5, Day = 6, Gain = 7, Sex = "undefined", Sigma = 8, TimeStamp= DateTime.Now.AddDays(-2), Uid = "D2-T-T1", Uniformity=9 }
             };
        }

        [Fact]
        public void Send_AddToQueue()
        {
            var messages = dailyWeights.Take(1).ToList();
            var result = kafkaSender.Send(messages);
            Assert.True(result);
            cacheMock.Verify(v => v.Add(It.Is<KafkaMessage>((value) => AreEqual(messages[0], value))), Times.Once);
        }

        [Fact]
        public void Send_ProducerSuccess()
        {
            using (var syncEvent = new ManualResetEvent(false))
            {
                var messages = dailyWeights.Take(1).ToList();
                producerMock.Setup(s => s.ProduceAsync(It.IsAny<string>(), null, It.IsAny<byte[]>()))
                   .Returns(() =>
                   {
                       return Task.FromResult(new Message(Topic.DailyWeight.ToString(), 0, 0, null, new byte[] { }, new Timestamp(DateTime.Now, TimestampType.CreateTime), new Error(ErrorCode.NoError)));
                   });

                cacheMock.Setup(s => s.Remove(It.IsAny<KafkaMessage>())).Callback<KafkaMessage>((item) =>
                {
                    AreEqual(dailyWeights[0], item);
                    syncEvent.Set();
                });

                var result = kafkaSender.Send(messages);
                Assert.True(result);
                Assert.True(syncEvent.WaitOne(5000));
                cacheMock.Verify(v => v.Refresh(It.IsAny<KafkaMessage>()), Times.Never);
                Assert.True(kafkaSender.Connected);
            }
        }

        [Fact]
        public void Send_ProducerError()
        {
            using (var syncEvent = new ManualResetEvent(false))
            {
                producerMock.Setup(s => s.ProduceAsync(It.IsAny<string>(), null, It.IsAny<byte[]>()))
                   .Returns(() =>
                   {
                       return Task.FromResult(new Message(Topic.DailyWeight.ToString(), 0, 0, null, new byte[] { }, new Timestamp(DateTime.Now, TimestampType.CreateTime), new Error(ErrorCode.BrokerNotAvailable)));
                   });

                cacheMock.Setup(s => s.Refresh(It.IsAny<KafkaMessage>())).Callback<KafkaMessage>((item) =>
                {
                    AreEqual(dailyWeights[0], item);
                    syncEvent.Set();
                });

                var result = kafkaSender.Send(dailyWeights.Take(1).ToList());
                Assert.True(result);
                Assert.True(syncEvent.WaitOne(5000));
                cacheMock.Verify(v => v.Remove(It.IsAny<KafkaMessage>()), Times.Never);
                Assert.False(kafkaSender.Connected);
            }
        }

        [Fact]
        public void Send_DisconnectedProducer()
        {
            producerMock.Raise(m => m.OnError += null, this, new Error(ErrorCode.Local_AllBrokersDown));
            var result = kafkaSender.Send(dailyWeights.Take(2).ToList());
            Assert.False(result);
        }

        [Fact]
        public void Send_ProducerException()
        {
            using (var syncEvent = new ManualResetEvent(false))
            {
                producerMock.Setup(s => s.ProduceAsync(It.IsAny<string>(), null, It.IsAny<byte[]>()))
                   .Throws(new Exception("Unit test exception"));
                cacheMock.Setup(s => s.Refresh(It.IsAny<KafkaMessage>())).Callback<KafkaMessage>((item) =>
                {
                    AreEqual(dailyWeights[0], item);
                    syncEvent.Set();
                });

                var result = kafkaSender.Send(dailyWeights.Take(1).ToList());
                Assert.True(result);
                Assert.True(syncEvent.WaitOne(5000));
                Assert.False(kafkaSender.Connected);
            }
        }

        [Fact]
        public void Dispose_Test()
        {
            kafkaSender.Dispose();
            Assert.True(cancellationTokenSource.IsCancellationRequested);
            cacheMock.Verify(v => v.Dispose(), Times.Once);
            producerMock.Verify(v => v.Dispose(), Times.Once);
        }

        private static bool AreEqual(DailyWeight dailyWeight, KafkaMessage message)
        {
            var byteMessage = JsonSerializer.FakeUtcAsBytes(dailyWeight);
            Assert.Equal(Topic.DailyWeight.ToString(), message.Topic);
            Assert.True(byteMessage.SequenceEqual(message.Message));
            return true;
        }
    }
}
