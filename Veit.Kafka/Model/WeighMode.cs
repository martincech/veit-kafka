﻿namespace Veit.Kafka.Model
{
   public enum WeighMode
   {
      Curve,
      AutomaticSlow,
      AutomaticFast
   }
}
