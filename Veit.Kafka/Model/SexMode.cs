﻿using System;
using Veit.Bat.Common;

namespace Veit.Kafka.Model
{
    public enum SexMode : byte
    {
        Undefined,
        Male,
        Female,
        Mixed
    }

    public static class MapSexMode
    {
        public static Sex Map(this SexMode sexMode)
        {
            switch (sexMode)
            {
                case SexMode.Male:
                    return Sex.Male;
                case SexMode.Female:
                    return Sex.Female;
                case SexMode.Undefined:
                    return Sex.Undefined;
                default:
                    throw new ArgumentOutOfRangeException(nameof(sexMode));
            }
        }

        public static SexMode MapFrom(this Sex sex)
        {
            switch (sex)
            {
                case Sex.Male:
                    return SexMode.Male;
                case Sex.Female:
                    return SexMode.Female;
                case Sex.Undefined:
                    return SexMode.Undefined;
                default:
                    throw new ArgumentOutOfRangeException(nameof(sex));
            }
        }
    }
}
