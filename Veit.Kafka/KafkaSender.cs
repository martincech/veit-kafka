﻿using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Confluent.Kafka;
using System.Threading;
using Veit.Kafka.Cache;
using Veit.Kafka.Model;
using Veit.Kafka.Topics;

namespace Veit.Kafka
{
    public class KafkaSender : IKafkaSender, IDisposable
    {
        private Task consumer;
        private readonly IProducer producer;
        private readonly ICachedQueue<KafkaMessage> cache;
        private readonly CancellationTokenSource cancellationTokenSource;

        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public bool Connected { get; private set; }

        public KafkaSender(string brokerList, string cacheFile)
           : this(
                new Producer(new Dictionary<string, object> {
                 { "bootstrap.servers", brokerList },
                 { "socket.keepalive.enable", true }
                }),
                new CachedQueue<KafkaMessage>(new FileStorage(cacheFile)),
                new CancellationTokenSource())
        {
        }

        public KafkaSender(IProducer producer, ICachedQueue<KafkaMessage> cache, CancellationTokenSource tokenSource)
        {
            cancellationTokenSource = tokenSource;
            this.producer = producer;
            producer.OnError += Producer_OnError;
            producer.OnLog += Producer_OnLog;
            this.cache = cache;
            StartConsumer();
        }

        private static void Producer_OnLog(object sender, LogMessage e)
        {
            logger.Debug(e);
        }

        private void Producer_OnError(object sender, Error e)
        {
            if (e.Code != ErrorCode.Local_AllBrokersDown)
            {
                logger.Error(e);
            }
            else
            {
                logger.Warn(e);
                Connected = false;
                AddToCache($"_{Topic.Ping}", new
                {
                    TimeStamp = DateTime.Now,
                    Source = Environment.MachineName,
                    Message = e.Reason
                });
            }
        }

        private static byte[] Serialize<T>(T item)
           => JsonSerializer.FakeUtcAsBytes(item);

        public bool Send(Co2Message message)
           => SendRoutine(message);

        public bool Send(TemperatureMessage message)
           => SendRoutine(message);

        public bool Send(HumidityMessage message)
           => SendRoutine(message);

        public bool Send(BirdWeightMessage message)
           => SendRoutine(message);

        public bool Send(RawWeightMessage message)
           => SendRoutine(message);

        public bool Send(DeviceMessage message)
           => SendRoutine(message);

        public bool Send(List<DeviceGroup> messages)
           => Send(Topic.Groups.ToString(), messages);

        public bool Send(List<DailyWeight> messages)
           => Send(Topic.DailyWeight.ToString(), messages);

        public bool Send(List<DeviceMap> messages)
         => Send(Topic.DeviceMap.ToString(), messages);

        public bool Send<T, U>(T message) where T : TopicMessage<U>
          => SendRoutine(message);

        private bool SendRoutine<T>(TopicMessage<T> message)
        {
            if (message == null) return false;
            return Send(message.Topic.ToString(), message);
        }

        private bool Send<T>(string topic, List<T> messages)
        {
            if (!Connected) return false;
            foreach (var message in messages)
            {
                AddToCache(topic, message);
            }
            return true;
        }

        private bool Send<T>(string topic, T message)
        {
            if (!Connected) return false;
            AddToCache(topic, message);
            return true;
        }

        private void AddToCache<T>(string topic, T message)
        {
            var serializedMessage = Serialize(message);
            cache.Add(new KafkaMessage(topic, serializedMessage));
            logger.Debug($"Kafka cache add: {message.ToString()}");
        }

        private void StartConsumer()
        {
            consumer = Task.Factory.StartNew(Consume, cancellationTokenSource.Token);
            Connected = true;
        }

        private void Consume()
        {
            foreach (var message in cache.GetConsumingEnumerable(cancellationTokenSource.Token))
            {
                try
                {
                    if (cancellationTokenSource.IsCancellationRequested) break;
                    producer.ProduceAsync(message.Topic, null, message.Message)
                       .ContinueWith((task) =>
                       {
                           if (task.Result.Error.HasError)
                           {
                               logger.Debug($"KafkaSender - failed to send message.");
                               Connected = false;
                               cache.Refresh(message);
                           }
                           else
                           {
                               logger.Debug($"KafkaSender - message has been sent successfully.");
                               Connected = true;
                               cache.Remove(message);
                           }
                       });
                }
                catch (Exception e)
                {
                    Connected = false;
                    cache.Refresh(message);
                    logger.Error(e);
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                cancellationTokenSource.Cancel();
                try
                {
                    consumer.Wait();
                }
                catch (Exception e)
                {
                    logger.Error(e, "Cancellation of KafkaSender task.");
                }

                cancellationTokenSource.Dispose();
                consumer.Dispose();

                producer.Dispose();
                cache.Dispose();
            }
        }
    }
}
