﻿using System.Runtime.Serialization;

namespace Veit.Kafka.Topics
{
    [DataContract]
    public class KafkaBatMessage
    {
        [DataMember(Name = "TerminalSn", Order = 1)]
        public string TerminalId { get; set; }
    }
    [DataContract]
    public class Message<T> : KafkaBatMessage
    {
        [DataMember(Name = "Data", Order = 3)]
        public T Value { get; set; }

        public override string ToString()
        {
            return $"T:{TerminalId} {Value}";
        }
    }
}
