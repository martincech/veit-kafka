﻿namespace Veit.Kafka.Topics
{
   public interface ITopicRelated
   {
      Topic Topic { get; }
   }
}
