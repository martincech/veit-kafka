﻿using System;
using System.Runtime.Serialization;

namespace Veit.Kafka.Topics
{
    [DataContract]
    public class DailyWeight
    {
        /// <summary>
        /// Sensor unique id
        /// </summary>
        [DataMember]
        public string Uid { get; set; }

        /// <summary>
        /// TimeStamp
        /// </summary>
        [DataMember]
        public DateTimeOffset TimeStamp { get; set; }

        /// <summary>
        /// Number of birds
        /// </summary>
        [DataMember]
        public int Count { get; set; }

        /// <summary>
        /// Weighing day
        /// </summary>
        [DataMember]
        public int Day { get; set; }

        /// <summary>
        /// Average weight
        /// </summary>
        [DataMember]
        public double Average { get; set; }

        /// <summary>
        /// Daily gain
        /// </summary>
        [DataMember]
        public double Gain { get; set; }

        /// <summary>
        /// Standard deviation
        /// </summary>
        [DataMember]
        public double Sigma { get; set; }

        /// <summary>
        /// Coefficient of variation
        /// </summary>
        [DataMember]
        public double Cv { get; set; }

        /// <summary>
        /// Uniformity
        /// </summary>
        [DataMember]
        public double Uniformity { get; set; }

        /// <summary>
        /// Sex
        /// </summary>
        [DataMember]
        public string Sex { get; set; }

        /// <summary>
        /// Histogram
        /// </summary>
        [DataMember]
        public Histogram Histogram { get; set; }
    }
}
