﻿using Veit.Bat.Common.Sample.Humidity;

namespace Veit.Kafka.Topics
{
   public class HumidityMessage : TopicMessage<HumiditySample>
   {
      public override Topic Topic
      {
         get { return Topic.Humidity; }
      }
   }
}
