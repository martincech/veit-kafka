﻿using Veit.Bat.Common.Sample.Temperature;

namespace Veit.Kafka.Topics
{
   public class TemperatureMessage : TopicMessage<TemperatureSample>
   {
      public override Topic Topic
      {
         get { return Topic.Temperature; }
      }
   }
}
