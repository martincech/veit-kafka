﻿namespace Veit.Kafka.Topics
{
    public enum Topic
    {
        Co2,
        Temperature,
        Humidity,
        BirdWeight,
        RawWeight,
        Device,
        Groups,
        DailyWeight,
        Ping,
        DeviceMap
    }
}
