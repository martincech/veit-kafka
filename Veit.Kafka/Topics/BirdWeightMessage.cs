﻿using Veit.Bat.Common.Sample.Weight;

namespace Veit.Kafka.Topics
{
   public class BirdWeightMessage : TopicMessage<BirdWeight>
   {
      public override Topic Topic
      {
         get { return Topic.BirdWeight; }
      }
   }
}
