﻿using Veit.Bat.Common.Sample.Weight;

namespace Veit.Kafka.Topics
{
   public class RawWeightMessage : TopicMessage<WeightSample>
   {
      public override Topic Topic
      {
         get { return Topic.RawWeight; }
      }
   }
}
