﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Veit.Kafka.Topics
{
    [DataContract]
    public class Histogram
    {
        /// <summary>
        /// Weight difference between adjacent steps in grams
        /// </summary>
        [DataMember]
        public float StepsSize { get; set; }

        /// <summary>
        /// Total number of steps in historgram
        /// </summary>
        [DataMember]
        public int StepsCount { get; set; }

        /// <summary>
        /// Center value of the histogram in grams
        /// </summary>
        [DataMember]
        public float Center { get; set; }

        /// <summary>
        /// Dictionary of histogram values. (Key = Step or Weight, Value = Count)
        /// </summary>
        [DataMember]
        public Dictionary<int, int> Steps { get; set; }
    }
}
