﻿using System;
using System.Runtime.Serialization;

namespace Veit.Kafka.Topics
{
    [DataContract]
    public class DeviceMap
    {
        [DataMember]
        public string Uid { get; set; }

        [DataMember]
        public Guid? Id { get; set; }

        [DataMember]
        public string Unit { get; set; }
    }
}
