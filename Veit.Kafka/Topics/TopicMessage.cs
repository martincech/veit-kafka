﻿using System.Runtime.Serialization;

namespace Veit.Kafka.Topics
{
    [DataContract]
    public abstract class TopicMessage<T> : Message<T>, ITopicRelated
    {
        public abstract Topic Topic { get; }

        #region Overrides of Message<T>

        public override string ToString()
        {
            return $"{Topic} {base.ToString()}";
        }

        #endregion
    }
}
